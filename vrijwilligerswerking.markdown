---
layout: default
title: Vrijwilligerswerking
background: BACKGR-suyana-7.jpg
---

Vele handen maken licht werk. Zowel in Peru als in België kan jij een steentje bijdragen!

## In Peru
Reeds verschillende vrijwilligers lieten België voor enige tijd achter zich om de realiteit in Lima te ontdekken.
In juli 2012 ging Bruno samen met 12 studenten van de KU Leuven 4 weken vrijwilligerswerk doen in Villa el Salvador. Concreet bouwden zij, samen met de lokale bevolking, 12 noodwoningen voor de allerarmsten in de sloppenwijk. Ook hielpen ze mee in de cuna (het kinderdagverblijf) en in de volkskeukens.

Lotte, klinisch psychologe van opleiding, ging van oktober 2011 tot oktober 2012 in Lima wonen en draaide enkele dagen per week mee in het kinderdagverblijf en op de psychosociale dienst van het medisch centrum.

Heb jij ook zin om voor enige tijd naar Lima te gaan om een handje toe te steken bij de  projecten die we steunen? Neem dan gerust contact met ons op, dan bekijken we samen wat de mogelijkheden zijn.

## In België
Heb je een idee?  Wil je een activiteit organiseren?  Of zie je jezelf eerder meehelpen op een event?  Kan je ons administratief ondersteunen? Wil je graag mee nadenken over het project? Of ben je een vlotte spreker en wil je het project graag voorstellen?
Je hulp is meer dan welkom. Neem gerust contact op… of schrijf je in.