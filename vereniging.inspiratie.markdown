---
layout: default
title: De vereniging - Inspiratie
background: BACKGR-suyana-2.jpg
---

## Missie

De vzw heeft tot doel het welzijn van de arme bevolking in Peru te helpen bevorderen en daarover bewustmakingscampagnes te voeren in het noorden. ‘Suyana’ (lokale taal in Peru: het quechua) betekent HOOP. De huidige focus ligt op de ondersteuning van diverse projecten ter bevordering van het welzijn van de armste inwoners van de sloppenwijk Villa el Salvadór in Lima.

De hulp die wij beogen is niet plaatsvervangend. Het moet hulp zijn die de eigen mogelijkheden ondersteunt en toelaat om deze op eigen kracht verder te ontwikkelen. Materiële hulp heeft dus maar een kleiner aandeel in onze plannen.

Een belangrijke focus is het helpen opbouwen van eigen teams van psychosociale helpers (psychologen en sociaal werkers, in Peru gevormd).
Dit werk is naast ‘zorgend’ ook en vooral preventief. Goede bijkomende en permanente vorming draagt hier sterk in bij.