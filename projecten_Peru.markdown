---
layout: default
title: Projecten in Peru - Achtergrond
background: BACKGR-suyana-3.jpg
---

## Villa El Salvador als district

“In [1971](http://nl.wikipedia.org/wiki/1971) bezetten tweehonderd dakloze families afkomstig uit het hooglandgebied van
de regio’s Ayacucho en Apurimac een stuk braakliggend land nabij [Lima][1] in
[Peru](http://nl.wikipedia.org/wiki/Peru).

Zij waren het rondtrekken moe en wilden een vaste plek om te wonen.
De regering, die het stuk land bestemd had voor grote projecten, zag hier niets in. De [pioniers](http://nl.wikipedia.org/wiki/Pionier)
waren echter vastbesloten te blijven. Vele anderen, onder de indruk door deze actie, sloten zich aan en weldra waren het 9000
gezinnen.

Na lang onderhandelen met de [regering](http://nl.wikipedia.org/wiki/Regering), kregen zij een nieuw en groter stuk
land, een zandvlakte aan de rand van [Lima][1]. Vol goede moed doopten de families hun nieuwe woonplaats
'Villa El Salvador': de Stad van de [Verlosser][2].“


[1]: http://nl.wikipedia.org/wiki/Lima_(Peru)
[2]: http://nl.wikipedia.org/wiki/Jezus_(traditioneel-christelijk_benaderd)