---
layout: default
title: De vereniging - Historiek
background: BACKGR-suyana-2.jpg
---

Suyana Peru is gegroeid uit persoonlijk engagement van enkelen tegen de achtergrond van een hele geschiedenis van
universitaire samenwerking.

## De universitaire context en achtergrond

De KU Leuven kent een lange traditie van samenwerking met Zuid-Amerika. Sinds de jaren 50 en 60 van de vorige eeuw komen
er regelmatig jonge academici naar België om in diverse domeinen te studeren in Leuven. Ook uit Peru waren er heel wat
studenten.

In 1980 begon de terroristische strijd van Sendero Luminoso. De reactie van het staatsapparaat was even gewelddadig en
zette deze samenwerking op een laag pitje. Deze interne oorlog duurde officieel tot 2000. Eind van de jaren 80 deden
enkele van die jonge academici via Prof. Jos Corveleyn beroep op de Leuvense Faculteit Psychologie en Pedagogische
wetenschappen voor het uitbouwen van een nieuwe opleiding in de psychologie. Dit leidde tot een intense samenwerking
met de Universidad de Lima tussen 1995 – 2004 (uitwisseling van docenten, updating van jonge academici, doctoraten,
praktisch samenwerken in volksbuurten). Dit werd mogelijk gemaakt door Belgische ontwikkelingssteun in samenwerking
met de Vlaamse Interuniversitaire Raad (VLIR).

Deze samenwerking creëerde de ruime context voor een samenwerking die tot op vandaag zeer actief is. Sinds 1997 zijn er
elk jaar laatstejaarsstudenten van de Faculteit Psychologie en Pedagogische Wetenschappen KU Leuven die naar Lima
trekken voor hun praktijkstage . In totaal hebben er reeds meer dan 80 studenten deze stage gedaan. Zij deden in Lima
veel levenservaring en beroepservaring op in moeilijke omstandigheden. De psychologie en de pedagogiek ten dienste van
hen die het meest nood hebben. ‘Bevrijdingspsychologie’, naar het woord van Ignacio Martin-Baró. Mede met de financiële
steun van de KU Leuven zijn er tussen 2000 en 2010 ook een twaalftal doctoraten gemaakt in Leuven. Deze jonge doctorandi
zijn nu ongeveer allemaal als junior professoren aan het werk in diverse departementen psychologie in de metropool Lima.
Zij vormen voor ons een stevig netwerk voor lokale, goed onderbouwde samenwerking.

## De recente evolutie

Eén van deze oud-stagiairs Psychologie, Liesbet Willems, trekt na haar stage voor langere tijd naar Peru om daar te
werken. In 2008 vertrekt zij naar Ayacucho en engageert zich fulltime in een NGO die daar werkt voor de opvang van
straatkinderen, allen slachtoffers van familiegeweld in een context van aanhoudend structureel geweld (politiek geweld
en langdurige psychosociale gevolgen van oorlogsgeweld).


Zij werkt daar aan psychosociale begeleiding en counseling van jonge slachtoffers en hun families, begeleiding van hen
die psychiatrische stoornissen opliepen hierdoor, aan de supervisie van het team van de lokaal opgeleide sociale
werksters en aan zoveel meer.
Vanaf 2012 zet zij haar know how in, in de hoofdstad Lima (zie onder c).

Daarnaast zijn er recentelijk verschillende andere initiatieven aan de gang:

Tussen 2008 en 2014: achtereenvolgens zijn er kleine projecten in uitvoering omtrent nauwkeurig gefocuste doelen: eerst
is er een boek uitgewerkt waarin de ervaring is samengebracht van diverse instanties (NGO’s en officiële diensten) over
‘goede praktijken’ in de zorg voor slachtoffers van familiegeweld dat gelinkt is aan de gevolgen van het politieke
geweld in het land (publicatie in 2011).

Nu, in 2013, loopt er een trainingsprogramma voor vrijwilligers en medewerkers van NGO’s en officiële diensten in de
provinciestad Ayacucho, in het hoogland van de Andes, de bakermat van het terrorisme en de interne oorlog. Dit is een
samenwerking van KU Leuven met twee universiteiten in Lima (Pontificia Universidad Catolica del Peru en Universidad
Antonio Ruiz de Montoya) en met diverse instanties in deze stad, alsook met de lokale, door de interne oorlog erg
gehavende, universiteit: Universidad Nacional San Cristobal de Huamanga. Deze projecten worden gefinancierd door
VLIR (Vlaamse gemeenschap) en door privé-sponsoring.

## Actueel

Vanaf 2009 is er voorts concrete samenwerking gegroeid met de Parochie Cristo El Salvador, in het zuidelijk district van Lima, Villa El Salvador.
Collega Prof. Em. Luis Ramirez Aguirre (psycholoog en advocaat, professor aan Universidad de Lima) is de contactpersoon. De eerste vraag kwam naar concrete steun vanuit België voor enkele basisnoden in deze zeer arme omgeving. Het district Villa El Salvador vormt één geheel met de districten Villa Maria del Triunfo en San Juan de Miraflores (samen ongeveer 1.5 miljoen inwoners, allen migranten (vanaf 1973) uit het Peruviaanse binnenland en hun gezinnen.  De meeste inwoners van het district behoren tot de lage middenklasse of arme arbeidersklasse en leven in extreme armoede.  De parochie is historisch het oudste gedeelte van deze migratiewijken en staat onder leiding van pastoor Cristobal Mejia, die solidariteit hoog op de agenda plaatst.

- 2010:
Bruno Spriet doet vrijwilligerswerk in de parochie. Met Vlaamse steun (vzw. Helpende Handen (Waasland)) en via privé-giften: uitbouw en bouw van volksgaarkeukens, ‘comedores’. Preventie van ondervoeding bij de allerarmsten, vooral jonge moeders en kinderen, ouderen en chronisch psychiatrische patiënten.
- 2011:
2 vrijwilligers, oud-studenten psychologie/pedagogiek werken mee aan de zorg voor chronisch psychiatrische patiënten (Brenda Van Rillaer en Heleen Haine)  
- 2011 – 2012:
een vrijwilliger, oud-student psychologie (Lotte Van Caudenberg) werkt mee in dezelfde context en zet zich in voor het kinderdagverblijf van de parochie (de Cuna)  
- 2012:
Liesbet Willems begint psychosociaal werk in het psychologisch centrum van de parochie, samen met Karen Yearwood, en sticht de NGO Padma.
- 2012 Juni:
Oprichting Suyana Peru vzw.
- 2012 Juli:
Inleefreis van de Universitaire Parochie (KU Leuven), o.l.v. Laura Deferm en Bruno Spriet: een soort van bouwkamp voor de constructie van 12 noodhuizen in de parochie en medewerking in kinderdagverblijf en in de ‘comedores’.
- 2012 augustus:
start van stages (psychologen en pedagogen van KU Leuven) in samenwerking met Universidad Femenina del Sagrado Corazon, Lima.