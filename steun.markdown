---
layout: default
title: Steun
background: BACKGR-suyana-6.jpg
---

Je kan ons steunen door, éénmalig of bijvoorbeeld maandelijks, een gift te storten op rekeningnr   BE52 0016 7379 4109.

Wil je een fiscaal attest: dit kan.  Op jaarbasis is het minimum bedrag 40 euro.  Dan stort je op het rekeningnrummer BE71 4569 5241 8169, BIC KREDBEBB, Stelimo vzw, Tulpinstraat 75, 3500 Hasselt met mededeling Liesbet Willems – Peru.

Heb je iets te vieren? En je wil je feestvreugde delen met mensen uit de derde wereld?  In plaats van geschenken kan je vragen om het project te steunen.
Je kan de vzw ook vermelden in je nalatenschap.  Voor meer informatie kan je bij ons terecht maar je kan ook bij de notaris terecht.
Ook als bedrijf kan je steunen.  We spreken graag met je de concrete modaliteiten af.
Ook als organisatie en als school kan je een steentje bijdragen.
Wij geven informatie over het project en jij zet een actie op.  Heb je een idee?  We horen het graag.

De vzw vraagt ook steun aan bij lokale en provinciale besturen.  Hiervoor dienen we projectaanvragen in.
